class AdherentType {

    constructor(name) {
        this.name = name;
        this.translatedName = this.constructor.translateTypeName(name);
        this.occurencies = 0;
    }

    static translateTypeName(name) {
        switch (name) {
            case "0_tous":
                return "Tous les adhérents";
            case "1_separator":
                return "--------------------------------------------------";
            case "Etablissement public":
                return "Établissement public";
            case "EPCI":
                return '<abbr title="Établissement public de coopération intercommunale">EPCI</abbr>';
            case "CDG":
                return "Centre de gestion";
            case "Département":
                return "Conseil départemental";
            case "Groupement public (GIE, GIP, SIH, …)":
                return 'Groupement public (<abbr title="Groupement d\'Intérêt Économique">GIE</abbr>, <abbr title="Groupement d\'Intérêt Public">GIP</abbr>, SIH, …)';
            case "SDIS":
                return '<abbr title="Service départemental d\'incendie et de secours">SDIS</abbr>';
            case "Education":
                return "Éducation";
            case "Association coll.":
                return "Association collectivités";
            // API Adresse
            case "street":
                return "rue";
            case "locality":
                return "lieu-dit";
            case "municipality":
                return "municipalité";
            default:
                return name;
        }
    }
}
