<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Adherent;

/**
 * Class AdherentsSpreadsheetCommand
 *
 * @package App\Command
 */
final class AdherentsSpreadsheetCommand extends Command
{
    /**
     * In this method setup command, description, and its parameters
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('adherentsspreadsheet');
        $this->setDescription('Convertit un fichier de type tableau des adhérents adullact vers un fichier CSV type API Adresse');
        $this->addArgument('inputfilename', InputArgument::REQUIRED, "Fichier d'entrée, format tableau des adhérents adullact");
        $this->addArgument('outputfilename', InputArgument::REQUIRED, "Fichier de sortie, format API Adresse");
    }

    /**
     * Convert CSV file, adherents spreadsheet format into CSV file, API Adresse format
     *
     * @param InputInterface  $input  Symfony console input
     * @param OutputInterface $output Symfony console output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputfilename = $input->getArgument('inputfilename');
        $outputfilename = $input->getArgument("outputfilename");
        $logger = new ConsoleLogger($output);

        $logger->notice("Converting " . $inputfilename . " to API adresse format…");
        $adherents = $this::adherentsSpreadsheetToArray($inputfilename, $logger);
        $this::adherentsArrayToCSV($adherents, $outputfilename, $logger);
        $logger->notice("Converting done.");

        return self::SUCCESS;
    }

    /**
     * Convert adherents spreadsheet into an array of adherents
     *
     * @param string          $inputfilename adherents spreadsheet filename (csv format)
     * @param LoggerInterface $logger        Symfony console logger
     *
     * @return Adherent[] an array of adherents
     */
    public static function adherentsSpreadsheetToArray(string $inputfilename, LoggerInterface $logger): array
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        $rows = $serializer->decode(file_get_contents($inputfilename), "csv");

        $adherents = [];
        foreach ($rows as $row) {
            $dateFirstMembership = substr($row["Date 1ere adhésion"], 6);
            if (empty($dateFirstMembership)) {
                $dateFirstMembership = null;
            }

            $adh = new Adherent($row["Adhérent"], $row["Adresse"], $row["Code Postal"], $row["Ville"], $row["Type adherent"], null, null, $dateFirstMembership);
            if ($adh->type == "Personne") {
                // Persons are ignored as requested not to leak the address
                $logger->info("Ignored " . $adh->nom . " (type: " . $adh->type . ")");
            } elseif ($adh->adresse == "(vide)" || empty(str_replace(" ", "", $adh->adresse))) {
                $logger->warning("Address is empty. Ignoring \"" . $adh->nom . "\"…");
            } else {
                array_push($adherents, $adh);
                $logger->debug('Added ' . $adh->nom . " (type: " . $adh->type . ")");
            }
        }
        return $adherents;
    }

    /**
     * Write adherents to API Adresse CSV format
     *
     * @param Adherent[]      $adherents      an array of adherents
     * @param string          $outputfilename (API Adresse CSV format, as defined in Documentation/geocodage.md)
     * @param LoggerInterface $logger         Symfony console logger
     *
     * @return void
     */
    public static function adherentsArrayToCSV(array $adherents, string $outputfilename, LoggerInterface $logger): void
    {
        $outputfile = fopen($outputfilename, "w");
        fputcsv($outputfile, ["nom", "adresse","postcode", "city", "input_type", "date_first_membership"]);
        foreach ($adherents as $adh) {
            fputcsv($outputfile, [$adh->nom, $adh->adresse, $adh->postcode, $adh->city, $adh->type, $adh->dateFirstMembership]);
            $logger->debug('Added line to CSV file: ' . $adh->nom . " (type: " . $adh->type . ")");
        }
        fclose($outputfile);
    }
}
