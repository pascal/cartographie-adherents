<?php

declare(strict_types=1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * Class GeocodeCommand
 *
 * @package App\Command
 */
final class GeocodeCommand extends Command
{
    /**
     * In this method setup command, description, and its parameters
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('geocode');
        $this->setDescription("Géocode un ensemble d'adresse, en utilisant le format CSV de l'API Adresse. Voir la documentation pour plus d'informations.");
        $this->addArgument('inputfilename', InputArgument::REQUIRED, "Fichier d'entrée, format CSV API Adresse");
        $this->addArgument('outputfilename', InputArgument::REQUIRED, "Fichier de sortie, format CSV API Adresse");
    }

    /**
     * Geocode a set of addresses
     *
     * Arguments:
     * - inputfilename: CSV file, API Adresse type (see Documentation/geocodage.md)
     * - outputfilename: CSV file
     *
     * @param InputInterface  $input  Symfony console input
     * @param OutputInterface $output Symfony console output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputfilename = $input->getArgument('inputfilename');
        $outputfilename = $input->getArgument("outputfilename");
        $logger = new ConsoleLogger($output);
        $client = HttpClient::create();

        $formFields = [
            "data" => DataPart::fromPath($inputfilename),
        ];
        $formData = new FormDataPart($formFields);

        $response = $client->request(
            "POST",
            "https://api-adresse.data.gouv.fr/search/csv/",
            [
            "headers" => $formData->getPreparedHeaders()->toArray(),
            "body" => $formData->bodyToIterable(),
            ]
        );

        $logger->notice("Sending request to API adresse…");
        file_put_contents($outputfilename, $response->getContent());
        $logger->notice("Response written to " . $outputfilename);
        return self::SUCCESS;
    }
}
