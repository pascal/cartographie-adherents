<?php

declare(strict_types=1);

namespace Tests;

use Psr\Log\NullLogger;
use PHPUnit\Framework\TestCase;
use App\Command\AdherentsSpreadsheetCommand;
use App\Adherent;

/**
 * Tests related to adherents spreadsheet
 *
 * @see Documentation/geocodage.md for adherents spreadsheet (tableur des adhérents) explaination
 */
final class AdherentsSpreadsheetTest extends TestCase
{
    /**
     * Assert that adherents with type "Personne" are ignored
     *
     * @return void
     */
    public function testPersonsIgnored(): void
    {
        $adherents = AdherentsSpreadsheetCommand::adherentsSpreadsheetToArray("tests/AdherentsSpreadsheetPersonsIgnoredTest.csv", new NullLogger());
        foreach ($adherents as $adh) {
            $this->assertFalse($adh->type == "Personne");
        }

        $this->assertSame(1, count($adherents));
        $this->assertSame("Mairie de Paris", $adherents[0]->nom);
    }
}
