<?php

$finder = PhpCsFixer\Finder::create()
    ->notPath("src/Kernel.php")
    ->in("src/", "tests/", ".php-cs-fixer.php");

$config = new PhpCsFixer\Config();
return $config->setRules([
        "@PSR12" => true,
    ])
    ->setFinder($finder);
