# Cartographie des adhérents

Ce dépôt contient le code nécessaire à la cartographie des adhérents d'Adullact.

Cela permet de générer une carte du monde avec les adhérents représentés par un point.

En cliquant sur un adhérent, on peut obtenir des informations telles que le nom, le type (ville, département, EPCI…).

## Déploiement de l'exemple

Pour déployer et visualiser l'exemple, il faut suivre les étapes indiquées dans [Documentation/deploiement.md](Documentation/deploiement.md).
