# Exporter le tableur des adhérents au format CSV

L'export se fait manuellement.

- Aller dans l'onglet AdherentsEnCours
- Copier coller les adhérents dans un nouveau tableur (au 31/05/2021, le tableau avec les adhérents ne commence pas au début mais à la ligne 18).
- Cliquer sur Fichier -> Enregistrer Sous
- Modifier l'extension de fichier : choisir .csv

<img src="export_csv_tableur_adherents_1.jpg" width="900">  
<img src="export_csv_tableur_adherents_2.jpg" width="900">
