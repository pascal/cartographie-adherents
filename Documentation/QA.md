# Quality Assurance _(Assurance Qualité)_

Voici la liste des outils de QA installés par PHIVE :

- phpunit
- php-cs-fixer
- parallel-lint
- phpcs (detect violation of coding standard)
- phpcbf (automatically correct coding standard violations)

## Installation

Prérequis : installer PHIVE 0.14.5

```bash
wget -O phive.phar https://github.com/phar-io/phive/releases/download/0.14.5/phive-0.14.5.phar
wget -O phive.phar.asc https://github.com/phar-io/phive/releases/download/0.14.5/phive-0.14.5.phar.asc
gpg --keyserver pool.sks-keyservers.net --recv-keys 0x9D8A98B29B2D5D79
gpg --verify phive.phar.asc phive.phar
chmod +x phive.phar
sudo mv phive.phar /usr/local/bin/phive
```

Installation des outils :

```bash
cd GeocodageDonnees/
phive install --force-accept-unsigned
```

Installation des dépendances de `GeocodageDonnees` (pour lancer les tests) :

```base
composer install
```

## Utilisation

```bash
cd GeocodageDonnees/

# détection des erreurs de standard de code
tools/parallel-lint src/ tests/
tools/phpcs -p --standard=phpcs.xml

# correction des erreurs de standard de code
tools/php-cs-fixer fix --config=.php-cs-fixer.php
tools/phpcbf --standard=phpcs.xml

# tests unitaires
tools/phpunit
```
