# Géocodage de données

## Définition

> Le géocodage consiste à affecter des coordonnées géographiques (longitude/latitude) à une adresse postale.
>
> -- [Géocodage — Wikipédia](https://fr.wikipedia.org/wiki/G%C3%A9ocodage)

## API Adresse

Pour géocoder les adresses des adhérents, nous utilisons l'API Adresse
[(documentation)](https://geo.api.gouv.fr/adresse) qui utilise la [Base d'adresse Nationale (BAN)](https://adresse.data.gouv.fr/).

Cette API permet de géocoder une liste d'adresse grâce au endpoint `/search/csv`.

## Formats de données

### "Format API Adresse"

L'API Adresse prend en entrée un fichier CSV et donne en sortie un autre fichier CSV
qui contient les informations de géocodage (longitude et latitude).

Ces fichiers sont ce qu'on appelle dans le reste de la documentation au _format API Adresse_ (soit l'entrée nécessaire ou la sortie de l'API Adresse).

En entrée, ce fichier CSV doit **obligatoirement** comporter ces colonnes : `nom,adresse,postcode,city`. L'ordre n'importe pas.  
La cartographie affiche le type renseigné dans la colonne `input_type`, le type de retour de l'API Adresse (`result_type`) sinon.  
Un exemple est disponible dans `Exemples/APartirApiAdresse/input.csv`.

### Format tableur des adhérents

Le tableur des adhérents est un tableur qui contient ces colonnes :

`Adhérent,Date 1ere adhésion,Type adherent,Type d’adhésion,SIREN,#hab/sal.,Nom,Prénom,Statut représentant,Adresse,Ville,Code Postal,B.P.,Cedex,Somme - Montant cotis.,département,mois anniv.,tranche ville/EPCI`.

## Géocoder des données 

Voir comment géocoder des données dans [deploiement.md](deploiement.md).
