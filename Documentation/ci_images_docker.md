# CI : Images Docker

Deux images Docker sont utilisées pour la CI :

- php_composer_phive : installe php 8.0 + Composer 2.0.14 + PHIVE 0.14.5 au niveau système
- dependencies : installe le reste des dépendances pour la CI

## Instructions pour publier les modifications des Dockerfiles

Voici les instructions publier les modifications des images Docker :

Prérequis :

```bash
docker login gitlab.adullact.net:4567
```

La version suit [SEMVER](https://semver.org/lang/fr/) et les instructions indiquées en dessous sont pour la `v1.0.0`.

### php_composer_phive

```bash
cd .gitlab/ci/Dockerfiles/php_composer_phive
docker build -t gitlab.adullact.net:4567/adullact/cartographie-adherents/php_composer_phive:v1.0.0 .
docker push gitlab.adullact.net:4567/adullact/cartographie-adherents/php_composer_phive:v1.0.0
```

### dependencies

```bash
cd .gitlab/ci/Dockerfiles/dependencies
docker build -t gitlab.adullact.net:4567/adullact/cartographie-adherents/dependencies:v1.0.0 .
docker push gitlab.adullact.net:4567/adullact/cartographie-adherents/dependencies:v1.0.0
```
