# Journalisation


## Présentation

Nous utilisons l'outil de journalisation de Symfony : Monolog.

Cet outil permet d'informer avec différents niveaux de verbosité :

- `$logger->error()` permet d'afficher des erreurs vers l'erreur standard `stderr`.
- `$logger->warning()` permet d'avertir l'utilisateur en utilisant la sortie standard `stdout`.
- `$logger->notice()` permet d'informer l'utilisateur en utilisant la sortie standard, si l'option `-v` est utilisée.
- `$logger->info()` permet d'informer l'utilisateur en utilisant la sortie standard, si l'option `-vv` est utilisée.
- `$logger->debug()` permet d'informer l'utilisateur ou le développeur afin d'aider au débuggage, si l'option `-vvv` est utilisée.

`warning` est utilisé pour avertir l'utilisateur de problèmes qui ne bloquent toutefois pas le processus.
Par exemple, un adhérent sans adresse sera ignoré et l'utilisateur sera prévenu, mais le programme continue de fonctionner pour le reste des adhérents.  
`notice` est utilisé pour indiquer les changements de tâches (conversion, géocodage…).  
`info` est utilisé pour par exemple pour informer les utilisateurs que les adhérents de type `Personne` sont ignorés.

## Documentation

- Utilisation de Monolog avec Symfony Console : [How to Configure Monolog to Display Console Messages](https://symfony.com/doc/current/logging/monolog_console.html)
- [Logging - Symfony](https://symfony.com/doc/current/logging.html)